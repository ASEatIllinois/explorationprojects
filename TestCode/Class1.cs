﻿using System;
using Microsoft.Pex.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestCode
{
    public class TestCode
    {
        [TestMethod]
        public void NoAssertion(int y)
        {
            int value = CodeUnderTest.CodeUnderTest.Compute(y);

            Console.Out.WriteLine("after computation, value is: " + value);

            if (value > 5)
            {
                Console.WriteLine("value > 5!");
            }
        }

        [TestMethod]
        public void VSAssertion(int y)
        {
            int value = CodeUnderTest.CodeUnderTest.Compute(y);
            Assert.IsTrue(value > 5);
        }

        [TestMethod]
        public void PexAssertion(int y)
        {
            int value = CodeUnderTest.CodeUnderTest.Compute(y);
            PexAssert.IsTrue(value > 5);
        }

        [TestMethod]
        public void AssertionBranch(int y)
        {
            // There are two methods to detect if an EMC affects an assertion or not. 
            // 1) Use Covana in its current way which only analyzes the current path and requires that we either transform test assertions to if statements or change Covana to recognize assertions as branch statements.
            // 2) For each EMCP we detect, we change the return value of the EMCP to be symbolic and apply Pex.
            // Try mocking things with Pex. Talk to Angello. Read Tao's 2014 paper. How to replace a return value from an EMC with a symbolic input.
            // Determine if we should transform assertions into if statements or if we should change Covana to treat assertions as branch statements.

            // What lines of code is determining that if a test does not have symbolic inputs then the test should not be considered for EMCPs? See PathConditionsAdded in ResultTrackingObserver.cs.
            // Create a Pex extension project that will make use of Covana to detect in a CUT that contains dynaimic data dependencies and one that does not contain dynamic data dependecies.
            int value = CodeUnderTest.CodeUnderTest.Compute(y);
            if (!(value > 5))
            {
                throw new Exception();
            }
        }
    }
}