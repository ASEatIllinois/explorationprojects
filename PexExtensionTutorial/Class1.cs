﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Pex.Engine.ComponentModel;
using Microsoft.ExtendedReflection.ComponentModel;
using Microsoft.Pex.Engine.PostAnalysis;
using Microsoft.Pex.Engine.Drivers;
using Microsoft.Pex.Engine.Exceptions;
using Microsoft.Pex.Engine.Choices;

using Microsoft.ExtendedReflection.Collections;
using Microsoft.ExtendedReflection.Interpretation;
using Microsoft.ExtendedReflection.Interpretation.Effects;
using Microsoft.ExtendedReflection.Interpretation.Visitors;
using Microsoft.ExtendedReflection.Emit;
using Microsoft.ExtendedReflection.Metadata;
using Microsoft.ExtendedReflection.Utilities.Safe.Diagnostics;
using Microsoft.ExtendedReflection.Utilities.Safe.IO;

using Microsoft.ExtendedReflection.Metadata.Names;
using Microsoft.Pex.Engine.Packages;
using Microsoft.Pex.Framework.ComponentModel;
using System.IO;

namespace PexExtensionTutorial
{
    public class PathAnalyzer
          : PexPathComponentBase, IService
    {
        PathsManager pathManager;

        protected override void Initialize()
        {
            this.pathManager = this.GetService<PathsManager>();
        }

        public void Analyze()
        {
            // Max level for Pex to explore. How many method calls before givining up
            int maxLevelCount = 32;
            for (int level = 0; level < maxLevelCount; level++)
            {
                ResultTracer controller = new ResultTracer(this, level);
                using (IEngine trackingEngine = this.PathServices.TrackingEngineFactory.CreateTrackingEngine(controller))
                {
                    IPexTrackingDriver driver = trackingEngine.GetService<IPexTrackingDriver>();
                    if (!driver.Run())
                        break;
                }
            }
        }

        private sealed class ResultTracer
            : PexTrackingControllerBase
        {
            int counter = 0;
            PathAnalyzer owner;
            int level;
            public ResultTracer(PathAnalyzer owner, int level)
            {
                this.owner = owner;
                this.level = level;
            }

            public override void FrameDisposed(int frameId, PexTrackingFrame frame)
            {
                bool firstPredicate = true;
                Term rawPassingPathCondition = this.TermManager.True;
                IEnumerable<Term> pathConditions = this.PathConditionBuilder.RawConditions;
                foreach (Term predicate in pathConditions)
                {
                    if (firstPredicate)
                    {
                        rawPassingPathCondition = predicate;
                        firstPredicate = false;
                    }
                    else
                    {
                        rawPassingPathCondition = this.owner.pathManager.termManager.LogicalAnd(predicate, rawPassingPathCondition);
                    }
                }
                this.owner.pathManager.AddRawGoodPathList(
                                       frame.Method,
                                       rawPassingPathCondition);
            }

            public override PexArgumentTracking GetArgumentTreatment(PexTrackingThread thread, int frameId,
                                                         Method method)
            {
                return PexArgumentTracking.Track;
            }

            public override int GetNextFrameId(int threadId, Method method)
            {
                counter += 1;
                return counter;
            }

            public override void PathConditionAdded(Term condition, int codeLabel)
            {
                base.PathConditionAdded(condition, codeLabel);
            }

            public override int GetNextCallId(int threadId, int offset, IMethodSignature methodSignature,
                                              TypeEx[] varArgTypes, Term[] arguments)
            {
                return base.GetNextCallId(threadId, offset, methodSignature, varArgTypes, arguments);
            }

            public override void DerivedArguments(int frameId, Method method, Term[] arguments)
            {
            }

            public override void FrameHasExceptions(int frameId, Method method)
            {
            }

            public override void FrameHasExplicitFailures(int frameId, Method method,
                                                          IEnumerable<object> exceptionObjects)
            {
            }
        }
    }

    partial class PathsManager : PexExplorationComponentBase, IService
    {
        SafeDictionary<Method, SafeList<Term>> rawGoodPathList = new SafeDictionary<Method, SafeList<Term>>();
        public TermManager termManager;
        TermEmitter emitter;
        TermSimplifier simplifier;
        protected override void Initialize()
        {
            this.termManager = this.ExplorationServices.TermManager;
            this.emitter = new TermEmitter(this.termManager, new NameCreator());    // MG: NameCreator Creates new names for objects
            this.simplifier = new TermSimplifier(this.termManager, true);
        }

        public void AddRawGoodPathList(Method method, Term pathList)
        {
            if (!this.rawGoodPathList.ContainsKey(method))
            {
                this.rawGoodPathList.Add(method, new SafeList<Term>());
            }
            this.rawGoodPathList[method].Add(pathList);
        }

        internal void dump(UniqueSubtermCounter subterms, TextWriter writer, Term value, TypeEx type)
        {
            try
            {
                IMethodBodyWriter codeWriter = this.Services.TestManager.Language.CreateBodyWriter(
                    writer,
                    VisibilityContext.Private,
                    100);
                if (subterms != null)
                    subterms.VisitTerm(default(TVoid), value);
                if (!this.emitter.TryEvaluate(
                    new Term[] { value },
                    10000, // bound on size of expression we are going to pretty-print
                    codeWriter))
                {
                    return;
                }

                codeWriter.Return(type);
            }
            catch (OutOfMemoryException)
            {
                writer.WriteLine("OutOfMemoryException");
            }
        }

        public void printPaths()
        {
            using (UniqueSubtermCounter subterms = new UniqueSubtermCounter(termManager)) {
                foreach (var m in rawGoodPathList.Keys)
                {
                    SafeStringWriter sw = new SafeStringWriter();
                    File.AppendAllText("c:\\tempTutorials\\paths.txt", "method: "  + m.FullName);
                    foreach (var p in rawGoodPathList[m])
                    {
                        this.dump(subterms, sw, p, SystemTypes.Bool);
                        File.AppendAllText("c:\\tempTutorials\\paths.txt", sw.ToString());
                    }
                }
            }
        }
    }

    class UniqueSubtermCounter : TermInternalizingRewriter<TVoid>
    {
        public UniqueSubtermCounter(TermManager termManager)
            : base(termManager, OnCollection.Fail)
        {
        }
    }

    public sealed class TutorialAttribute
       : PexComponentElementDecoratorAttributeBase
       , IPexExplorationPackage, IPexPathPackage
    {
        bool isFirstRun = true;
        void IPexPathPackage.Load(IContainer pathContainer)
        {
            pathContainer.AddComponent("PathAnalyzer", new PathAnalyzer());
        }

        object IPexPathPackage.BeforeRun(IPexPathComponent host)
        {
            return null;
        }

        void IPexPathPackage.AfterRun(IPexPathComponent host, object data)
        {
            PathAnalyzer pexumPathAnalyzer =
                ServiceProviderHelper.GetService<PathAnalyzer>(host.Site);
            pexumPathAnalyzer.Analyze();
        }

        void IPexExplorationPackage.Load(
            IContainer explorationContainer)
        {
            explorationContainer.AddComponent(
                "PathsManager",
                new PathsManager());
        }

        void IPexExplorationPackage.Initialize(
            IPexExplorationEngine host) { }

        object IPexExplorationPackage.BeforeExploration(
            IPexExplorationComponent host)
        {
            PathsManager pexumManager =
                ServiceProviderHelper.GetService<PathsManager>(host.Site);
            if (isFirstRun)
            {
                isFirstRun = false;
            }
            return null;
        }

        void IPexExplorationPackage.AfterExploration(
            IPexExplorationComponent host,
            object data)
        {
            PathsManager pexumManager =
                ServiceProviderHelper.GetService<PathsManager>(host.Site);
            pexumManager.printPaths();
        }

        protected sealed override void Decorate(Name location, IPexDecoratedComponentElement host)
        {
            SafeDebug.AssumeNotNull(location, "location");
            SafeDebug.AssumeNotNull(host, "host");

            host.AddExplorationPackage(location, this);
            host.AddPathPackage(location, this);

        }

        public string Name
        {
            get { return "Tutorial"; }
        }
    }

}