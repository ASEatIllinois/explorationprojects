﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestCode;
using Microsoft.Pex.Framework;

namespace CovanaTestProject
{
    [PexClass(typeof(ExternalMethodsReturnValueTest))]
    public partial class ExternalMethodsReturnValueTest
    {
        [PexMethod]
        public void TestNoAssertion(int y)
        {
            TestCode.TestCode obj = new TestCode.TestCode();
            obj.NoAssertion(y);
        }

        [PexMethod]
        public void TestVSAssertion(int y)
        {
            TestCode.TestCode obj = new TestCode.TestCode();
            obj.VSAssertion(y);
        }

        [PexMethod]
        public void TestPexAssertion(int y)
        {
            TestCode.TestCode obj = new TestCode.TestCode();
            obj.PexAssertion(y);
        }

        [PexMethod]
        public void TestAssertionBranch(int y)
        {
            TestCode.TestCode obj = new TestCode.TestCode();
            obj.AssertionBranch(y);
        }

        [PexMethod]
        public void TestNoAssertion1()
        {
            TestCode.TestCode obj = new TestCode.TestCode();
            obj.NoAssertion(1);
        }

        [PexMethod]
        public void TestVSAssertion1()
        {
            TestCode.TestCode obj = new TestCode.TestCode();
            obj.VSAssertion(1);
        }

        [PexMethod]
        public void TestPexAssertion1()
        {
            TestCode.TestCode obj = new TestCode.TestCode();
            obj.PexAssertion(2);
        }

        [PexMethod]
        public void TestAssertionBranch1()
        {
            TestCode.TestCode obj = new TestCode.TestCode();
            obj.AssertionBranch(2);
        }
    }
}
